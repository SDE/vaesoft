# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class Molflow(CMakePackage):
    """A Monte-Carlo Simulator package developed at CERN"""

    homepage = "https://molflow.web.cern.ch/"
    git      = "https://gitlab.cern.ch/molflow_synrad/molflow.git/"
    maintainers = ['ChristianTackeGSI']

    version('2.9.4_beta', commit='426e6ccc239a82cbd8a14f18be15cbbf07bd7e74', submodules=True, no_cache=True)
    version('2.9.3_beta', commit='26599088db87ddda2e3d1c5f6f9f2f93907873b9', submodules=True, no_cache=True)

    depends_on('cmake@3.12.2:', type='build')
    depends_on('openmpi', when='@2.9.4_beta')

    def cmake_args(self):
        args = [
                self.define('NO_INTERFACE', True)
                ]
        if self.spec.satisfies('@2.9.4_beta'):
            args.append('-DUSE_MPI=ON')
        return args

    def install(self, spec, prefix):
        mkdirp(prefix.bin)
        install(join_path(self.build_directory, 'bin', 'molflowCLI'),
                prefix.bin)
