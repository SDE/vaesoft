# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install fluka
#
# You can edit this file again by typing:
#
#     spack edit fluka
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

import os
from spack import *
import llnl.util.tty as tty


class Fluka(Package):
    """FIXME: Put a proper description of your package here."""

    homepage = "https://fluka.cern"
    url      = "file:///data.local2/local-pkgs/fluka/fluka-4-0.1.Linux-gfor9.tar"

    version('4-3.0.Linux-gfor9', sha256='ec67d9d89b59be1830076412d75803b3f2f0a0afcdab070e4711096190ad4cdd')
    version('4-2.2.Linux-gfor9', sha256='ec0f32c64fef797c7f304be9c81a5bfc15661a1dd154b6653e8480849d2fca7b')
    version('4-2.1.Linux-gfor9', sha256='8e85aeb8bb53c782f114233480080a1f2419dd3fd0ecc720c1cd078dc7d8faf7')
    version('4-2.0.Linux-gfor9', sha256='e23048d3b8dabb7accc907349fa459eb3a08f8efb8cc676a259026e3d5a0f494')
    version('4-1.1.Linux-gfor9', sha256='ad4224afa3557dfe1218c667759e77414763a42977e818f4b5780c3faa36c05f')
    version('4-1.0.Linux-gfor9', sha256='f52eea044b7f1b45798c6ee42769aa55784f98b86ea6315e3123023b879d858d')
    version('4-0.1.Linux-gfor9', sha256='34c0e327d5fa1e790b6a374217f5949b3fe762fd35253e62917860ec3e7807f2')

    def install(self, spec, prefix):
        with working_dir(os.path.join(self.stage.source_path, 'src')):
            make()
        install_tree('.', prefix)

    def setup_build_environment(self, env):
        env.set('I_HAVE_REGISTERED_FLUKA', '1')

    def setup_run_environment(self, env):
        if os.environ.get('I_HAVE_REGISTERED_FLUKA', None) != '1':
            tty.warn('Usage of FLUKA requires a per user registration.')
            tty.warn('Please register at:')
            tty.warn('    https://fluka.cern/download/registration')
            tty.warn('If you have registered')
            tty.warn('set the environment variable')
            tty.warn('I_HAVE_REGISTERED_FLUKA to 1')
            tty.die('Stopping now')
